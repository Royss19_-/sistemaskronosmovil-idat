class Cliente {
  Cliente({
    this.numberDocDNI,
    this.correo,
    this.idClient,
    this.direccion,
    this.referencia,
    this.idDistrito,
    this.celular,
    this.fullNameClient,
    this.idDNI,
    this.nameDocDNI,
    this.nameDistrito,
  });

  String numberDocDNI;
  String correo;
  String idClient;
  String direccion;
  String referencia;
  String idDistrito;
  String celular;
  String fullNameClient;
  String idDNI;
  String nameDocDNI;
  String nameDistrito;

  Cliente.fromJson(Map<String, dynamic> json) {
    numberDocDNI = json["numeroDocumentoIdentidad"];
    correo = json["correo"];
    idClient = json["idCliente"];
    direccion = json["direccion"];
    referencia = json["referencia"];
    idDistrito = json["idDistrito"];
    celular = json["celular"];
    fullNameClient = json["nombreCompleto"];
    idDNI = json["idDocumentoIdentidad"];
    nameDocDNI = json["nombreDocumentoIdentidad"];
    nameDistrito = json["nombreDistrito"];
  }
}
