import 'package:intl/intl.Dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:app_gustilandia/src/order/model/orders.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:app_gustilandia/src/order/services/order_service.dart';

class ListPedidosClient extends StatelessWidget {
  const ListPedidosClient();

  @override
  Widget build(BuildContext context) {
    final orderService = Provider.of<OrderService>(context);
    return FutureBuilder(
        future: orderService.getOnlyOrders(),
        builder:
            (BuildContext context, AsyncSnapshot<List<OrdersModel>> snapshot) {
          final xd = snapshot.data;
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: xd.length,
              itemBuilder: (context, index) {
                return _TargetPedidos(xd[index]);
              },
            );
          } else {
            return Center(
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
                strokeWidth: 5.0,
              ),
            );
          }
        });
  }
}

class _TargetPedidos extends StatelessWidget {
  final OrdersModel orders;
  const _TargetPedidos(this.orders);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          Navigator.pushNamed(context, 'details-order', arguments: orders),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          color: Colors.white,
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Colors.black26,
              blurRadius: 5.0,
              spreadRadius: 2.0,
              offset: Offset(2.0, 5.0),
            )
          ],
        ),
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _showHead(orders),
              SizedBox(height: 5.0),
              _totalItems(orders),
              SizedBox(height: 5.0),
              _totalPrice(orders),
              SizedBox(height: 15.0),
              _showDate(orders),
            ],
          ),
        ),
      ),
    );
  }
}

Widget _showDate(OrdersModel orders) {
  var fech = DateTime.parse(orders.fechaVentaGuardada);
  var formatter = new DateFormat('dd-MM-yyyy');
  String formatted = formatter.format(fech);
  return Row(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      Text(
        'Fecha de compra:',
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.start,
        style: TextStyle(
          color: Colors.grey,
          fontWeight: FontWeight.bold,
          fontStyle: FontStyle.italic,
          fontSize: 22,
        ),
      ),
      SizedBox(width: 5.0),
      Text(
        '$formatted',
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.start,
        style: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.w600,
          fontStyle: FontStyle.italic,
          fontSize: 22,
        ),
      )
    ],
  );
}

Widget _showHead(OrdersModel orders) {
  return Row(children: [
    Icon(
      FontAwesomeIcons.clipboard,
      size: 18,
      color: Colors.grey.shade600,
    ),
    Text(
      'Orden ID: ',
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      textAlign: TextAlign.start,
      style: TextStyle(
        color: Colors.grey,
        fontWeight: FontWeight.bold,
        fontSize: 18,
      ),
    ),
    SizedBox(width: 5.0),
    Text(
      '${orders.numeroVenta}',
      maxLines: 2,
      overflow: TextOverflow.ellipsis,
      textAlign: TextAlign.start,
      style: TextStyle(
        color: Colors.black,
        fontWeight: FontWeight.bold,
        fontSize: 18,
      ),
    ),
  ]);
}

Widget _totalItems(OrdersModel orders) {
  return Row(
    children: [
      Icon(
        FontAwesomeIcons.shoppingBag,
        size: 18,
        color: Colors.grey.shade600,
      ),
      Text(
        'Total items: ',
        style: TextStyle(
          color: Colors.grey,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
      Text(
        '${orders.cantidad} productos',
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        textAlign: TextAlign.start,
        style: TextStyle(
          color: Colors.red,
          fontWeight: FontWeight.w500,
          fontSize: 18,
        ),
      )
    ],
  );
}

Widget _totalPrice(OrdersModel orders) {
  return Row(
    children: [
      Icon(
        FontAwesomeIcons.coins,
        size: 15,
        color: Colors.grey.shade600,
      ),
      SizedBox(width: 5),
      Text(
        'Total: ',
        style: TextStyle(
          color: Colors.grey,
          fontWeight: FontWeight.bold,
          fontSize: 20,
        ),
      ),
      Text(
        'S/ ${orders.total}',
        style: TextStyle(
          color: Colors.blue,
          fontWeight: FontWeight.w500,
          fontSize: 20,
        ),
      ),
      Spacer(),
      _styleStatus(orders)
    ],
  );
}

Widget _styleStatus(OrdersModel orders) {
  Widget styleStatus;

  switch (orders.estado) {
    case 'Inicializado':
      styleStatus = _showEstatus('Inicializado', Colors.yellowAccent.shade700);
      break;
    case 'Pendiente':
      styleStatus = _showEstatus('Pendiente', Colors.purple);
      break;
    case 'Anulado':
      styleStatus = _showEstatus('Anulado', Colors.red);
      break;
    case 'En Proceso de despacho':
      styleStatus = _showEstatus('En Proceso de despacho', Colors.orange);
      break;
    case 'En Reparto':
      styleStatus = _showEstatus('En Reparto', Colors.blue);
      break;
    case 'Entregado':
      styleStatus = _showEstatus('Entregado', Colors.green);
      break;
    default:
      styleStatus = _showEstatus('Inicializado', Colors.yellowAccent.shade700);
      break;
  }

  return styleStatus;
}

Widget _showEstatus(String estado, Color color) {
  return Container(
    padding: EdgeInsets.all(7),
    width: 120,
    decoration: BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(10),
    ),
    child: Text(
      '$estado',
      textAlign: TextAlign.center,
      overflow: TextOverflow.ellipsis,
      style: TextStyle(
        color: Colors.white,
        fontSize: 17,
        fontWeight: FontWeight.bold,
      ),
    ),
  );
}
