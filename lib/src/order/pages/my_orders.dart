import 'package:flutter/material.dart';

import 'package:app_gustilandia/src/order/pages/list_pedidos_client.dart';

class MyOrders extends StatefulWidget {
  @override
  _MyOrdersState createState() => _MyOrdersState();
}

class _MyOrdersState extends State<MyOrders> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        backgroundColor: Colors.white,
        elevation: 7,
        centerTitle: true,
        title: Text(
          'Mis Ordenes',
          style: TextStyle(
            color: Colors.redAccent.shade200,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListPedidosClient(),
    );
  }
}
