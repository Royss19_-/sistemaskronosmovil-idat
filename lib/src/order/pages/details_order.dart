import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:app_gustilandia/src/order/model/orders.dart';
import 'package:app_gustilandia/src/order/model/orders_details.dart';
import 'package:app_gustilandia/src/order/services/order_service.dart';

class DetailsOrder extends StatefulWidget {
  const DetailsOrder();

  @override
  _DetailsOrderState createState() => _DetailsOrderState();
}

class _DetailsOrderState extends State<DetailsOrder> {
  @override
  Widget build(BuildContext context) {
    final OrdersModel orders = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body: SafeArea(
        child: ListView(
          children: [
            Row(children: [BackButton()]),
            SizedBox(height: 5),
            _titlePage(orders),
            SizedBox(height: 15),
            _statusPedido(orders),
            SizedBox(height: 5),
            _detailsPedido(context, orders),
            SizedBox(height: 5),
          ],
        ),
      ),
    );
  }

  Widget _titlePage(OrdersModel orders) {
    return Container(
      width: double.infinity,
      child: Text(
        'Detalle del pedido\n ${orders.numeroVenta}',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  Widget _statusPedido(OrdersModel orders) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 8.0),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 5.0,
            spreadRadius: 2.0,
            offset: Offset(2.0, 5.0),
          )
        ],
      ),
      child: _showWhileCase(orders),
    );
  }

  Widget _showWhileCase(OrdersModel orders) {
    Widget styleWhileCase;

    switch (orders.estado) {
      case 'Inicializado':
        styleWhileCase = _showImgAndText(
            'assets/images/inizializado.gif', 'Su pedido está siendo revisado');
        break;
      case 'Pendiente':
        styleWhileCase = _showImgAndText(
            'assets/images/pendiente.png', 'Su pedido se encuentra pendiente');
        break;
      case 'Anulado':
        styleWhileCase = _showImgAndText(
            'assets/images/anulado.png', 'Su pedido ha sido anulado');
        break;
      case 'En Proceso de despacho':
        styleWhileCase = _showImgAndText('assets/images/despacho2.png',
            'Su pedido está en proceso de despacho');
        break;
      case 'En Reparto':
        styleWhileCase = _showImgAndText(
            'assets/images/delivery.gif', 'Su pedido está en camino');
        break;
      case 'Entregado':
        styleWhileCase =
            _showImgAndText('assets/images/box.gif', 'Su pedido fue entregado');
        break;
      default:
        styleWhileCase = _showImgAndText(
            'assets/images/inizializado.gif', 'Su pedido está siendo revisado');
        break;
    }

    return styleWhileCase;
  }

  Widget _showImgAndText(String pathUrl, String text) {
    return Column(
      children: [
        Container(
          child: Image.asset(
            pathUrl,
            width: 100,
            height: 100,
          ),
        ),
        SizedBox(height: 5),
        Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }

  Widget _detailsPedido(BuildContext context, OrdersModel orders) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 8.0),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5.0),
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black26,
            blurRadius: 5.0,
            spreadRadius: 2.0,
            offset: Offset(2.0, 5.0),
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _logoInfo(),
          SizedBox(height: 15),
          _showDirection(orders),
          SizedBox(height: 15),
          Text(
            'TU PEDIDO',
            style: TextStyle(
              color: Colors.grey,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
          ),
          _showDetailsOrder(context, orders),
          SizedBox(height: 20),
          Divider(),
          _showPayment(orders),
        ],
      ),
    );
  }

  Widget _logoInfo() {
    return Row(
      children: [
        ClipOval(
          child: Image.asset(
            'assets/images/logo.jpg',
            width: 60,
            height: 60,
          ),
        ),
        SizedBox(width: 10),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Gustilandia SAC',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            SizedBox(width: 10),
            Text(
              'Monseñor Edward 1871, San Borja',
              //overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                color: Colors.grey,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget _showDetailsOrder(BuildContext context, OrdersModel orders) {
    final orderService = Provider.of<OrderService>(context);
    return FutureBuilder(
      future: orderService.getDetailsOrder(orders.idVenta),
      builder:
          (BuildContext context, AsyncSnapshot<List<OrdersDetails>> snapshot) {
        final lstDetails = snapshot.data;
        if (snapshot.hasData) {
          return ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            padding: EdgeInsets.all(4),
            itemCount: lstDetails.length,
            itemBuilder: (context, index) {
              final objDetails = lstDetails[index];
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '${objDetails.quantity}',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    'x',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    'S/ ${objDetails.price}',
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    ),
                  ),
                  Text(
                    '${objDetails.product}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: 18,
                    ),
                  ),
                  ClipOval(
                      child: FadeInImage(
                    image: NetworkImage(objDetails.getImagen(objDetails.image)),
                    width: 40,
                    height: 40,
                    placeholder: AssetImage(
                      'assets/images/giphy.gif',
                    ),
                  ))
                ],
              );
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(Colors.red),
              strokeWidth: 5.0,
            ),
          );
        }
      },
    );
  }

  Widget _showDirection(OrdersModel orders) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'DIRECCIÓN DE ENTREGA',
          style: TextStyle(
            color: Colors.grey,
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
        Text(
          '${orders.direccion}',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
      ],
    );
  }

  Widget _showPayment(OrdersModel orders) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'TU COBRO',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
        Text(
          'S/ ${orders.total}',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }
}
