import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:app_gustilandia/src/env/global.dart';
import 'package:app_gustilandia/src/order/model/orders.dart';
import 'package:app_gustilandia/src/order/model/order_model.dart';
import 'package:app_gustilandia/src/order/model/orders_details.dart';
import 'package:app_gustilandia/src/cliente/preferences/profile_preferences.dart';

class OrderService with ChangeNotifier {
  final _prefs = new PreferenciasUsuario();
  Future<List<Order>> getOrdersByClient() async {
    final List<Order> orders = [];
    final url = '${Global.URL_GUSTILANDIA}/venta/listarVentasCliente';
    final resp = await http.get(Uri.parse(url), headers: {
      'content-type': 'application/json',
      'Authorization': _prefs.getToken
    });

    if (resp.statusCode == 200) {
      String body = utf8.decode(resp.bodyBytes);
      final decodedData = json.decode(body);
      if (decodedData == null) return [];
      for (var item in decodedData["result"]) {
        print(decodedData["result"]);
        orders.add(Order.fromJson(item));
      }
    } else {
      return [];
    }
    print('servicio lista: $orders');
    return orders;
  }

  Future<List<OrdersModel>> getOnlyOrders() async {
    List<OrdersModel> orders = [];
    final url = '${Global.URL_GUSTILANDIA}/venta/ventasCliente';
    final resp = await http.get(Uri.parse(url), headers: {
      'content-type': 'application/json',
      'Authorization': _prefs.getToken
    });
    if (resp.statusCode == 200) {
      String body = utf8.decode(resp.bodyBytes);
      final decodedData = await json.decode(body);
      if (decodedData == null) return [];
      for (var item in decodedData["result"]) {
        orders.add(OrdersModel.fromJson(item));
      }
    } else {
      return [];
    }
    return orders;
  }

  Future<List<OrdersDetails>> getDetailsOrder(int id) async {
    final List<OrdersDetails> orders = [];
    final url = '${Global.URL_GUSTILANDIA}/venta/detalle/$id';
    final resp = await http.get(Uri.parse(url), headers: {
      'content-type': 'application/json',
      'Authorization': _prefs.getToken
    });
    if (resp.statusCode == 200) {
      String body = utf8.decode(resp.bodyBytes);
      final decodedData = await json.decode(body);
      if (decodedData == null) return [];
      for (var item in decodedData["result"]) {
        orders.add(OrdersDetails.fromJson(item));
      }
    } else {
      return [];
    }
    return orders;
  }
}
