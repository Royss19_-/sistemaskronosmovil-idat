class Order {
  Order({
    this.deliveryDate,
    this.numberSale,
    this.total,
    this.address,
    this.status,
    this.quantity,
    this.saleDateSave,
    this.details,
    this.idVenta,
  });

  String deliveryDate;
  String numberSale;
  String total;
  String address;
  String status;
  int quantity;
  String saleDateSave;
  List<OrderDetails> details;
  int idVenta;

  Order.fromJson(Map<String, dynamic> json) {
    deliveryDate = json["fechaEntrega"];
    numberSale = json["numeroVenta"];
    total = json["total"];
    address = json["direccion"];
    status = json["estado"];
    quantity = json["cantidad"];
    saleDateSave = json["fechaVentaGuardada"];
    details = List<OrderDetails>.from(
        json["ventaDetalle"].map((e) => OrderDetails.fromJson(e)));
    idVenta = json["idVenta"];
  }
}

class OrderDetails {
  OrderDetails({
    this.quantity,
    this.product,
    this.image,
    this.price,
  });

  int quantity;
  String product;
  String image;
  String price;

  OrderDetails.fromJson(Map<String, dynamic> json) {
    quantity = json["cantidad"];
    product = json["producto"];
    image = json["imagen"];
    price = json["precio"];
  }
}
