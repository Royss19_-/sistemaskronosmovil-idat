class OrdersModel {
  OrdersModel({
    this.total,
    this.fechaEntrega,
    this.numeroVenta,
    this.fechaVentaGuardada,
    this.estado,
    this.direccion,
    this.cantidad,
    this.idVenta,
  });

  String total;
  String fechaEntrega;
  String numeroVenta;
  String fechaVentaGuardada;
  String estado;
  String direccion;
  int cantidad;
  int idVenta;

  OrdersModel.fromJson(Map<String, dynamic> json) {
    fechaEntrega = json["fechaEntrega"];
    numeroVenta = json["numeroVenta"];
    total = json["total"];
    direccion = json["direccion"];
    estado = json["estado"];
    cantidad = json["cantidad"];
    fechaVentaGuardada = json["fechaVentaGuardada"];
    idVenta = json["idVenta"];
  }
}
