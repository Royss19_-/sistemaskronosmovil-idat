class OrdersDetails {
  OrdersDetails({
    this.quantity,
    this.product,
    this.image,
    this.price,
  });

  int quantity;
  String product;
  String image;
  String price;

  OrdersDetails.fromJson(Map<String, dynamic> json) {
    quantity = json["cantidad"];
    product = json["producto"];
    image = json["imagen"];
    price = json["precio"];
  }

  getImagen(String img) {
    return 'http://192.168.0.106:8085/backendgusti/files/img/producto/$img';
  }
}
