import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:app_gustilandia/src/env/global.dart';
import 'package:app_gustilandia/src/venta/services/shop_service.dart';
import 'package:app_gustilandia/src/cliente/preferences/profile_preferences.dart';

class VentaService with ChangeNotifier {
  String token = new PreferenciasUsuario().getToken;
  String messageError = "";

  Future<bool> registerVenta(List<ProductItem> lstProductos, cardName,
      cardNumber, cardExpirate, cardCvv) async {
    final authData = {
      //"correlativoComprobante": "",
      "detalleVenta": lstProductos
          .map((e) =>
              {"cantidad": e.quantity, "idProducto": e.product.idProducto})
          .toList(),
      "idTipoComprobanteSunat": 2,
      "idVenta": 0,
      "tarjeta": {
        "correo": "felipe@gmail.com",
        "cvv": cardCvv,
        "fechaVencimiento": cardExpirate,
        "nroTarjeta": cardNumber
      }
    };
    final resp = await http.post(Uri.parse('${Global.URL_GUSTILANDIA}/venta'),
        headers: {'content-type': 'application/json', 'Authorization': token},
        body: json.encode(authData));

    String body = utf8.decode(resp.bodyBytes);
    Map<String, dynamic> decodeResp = json.decode(body);

    if (decodeResp["result"] != null) {
      return true;
    } else {
      messageError = decodeResp["message"];
      return false;
    }
  }
}
