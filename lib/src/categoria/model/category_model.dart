import 'package:flutter/material.dart';

class CategoryModel {
  final int id;
  final IconData icon;
  final String name;

  CategoryModel(this.id, this.icon, this.name);
}
