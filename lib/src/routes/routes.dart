import 'package:app_gustilandia/src/order/pages/details_order.dart';
import 'package:flutter/material.dart';

import 'package:app_gustilandia/src/cliente/pages/edit_profile.dart';
import 'package:app_gustilandia/src/login/pages/login_page.dart';
import 'package:app_gustilandia/src/order/pages/my_orders.dart';
import 'package:app_gustilandia/src/general/navigation_bar.dart';
import 'package:app_gustilandia/src/login/pages/register_page.dart';
import 'package:app_gustilandia/src/venta/pages/finish_pay_page.dart';

//import 'package:app_gustilandia/src/general/tabs_page.dart';
import 'package:app_gustilandia/src/venta/pages/tab_shop.dart';
import 'package:app_gustilandia/src/cliente/pages/tab_profile.dart';
import 'package:app_gustilandia/src/producto/pages/tab_store.dart';
import 'package:app_gustilandia/src/producto/pages/details_product.dart';

Map<String, WidgetBuilder> getAplicationRoutes() {
  return <String, WidgetBuilder>{
    'login': (BuildContext context) => LoginPage(),
    //'tabs': (BuildContext context) => TabsPage(),
    'store': (BuildContext context) => TabStore(),
    'details': (BuildContext context) => DetailsProduct(),
    'details-order': (BuildContext context) => DetailsOrder(),
    'shop': (BuildContext context) => TabShop(),
    'profile': (BuildContext context) => TabProfile(),
    'register': (BuildContext context) => RegisterPage(),
    'edit_profile': (BuildContext context) => EditProfile(),
    'bagShoping': (BuildContext context) => MyOrders(),
    'navigation': (BuildContext context) => NavigationBar(),
    'finish-pay': (BuildContext context) => FinishPayPage()
  };
}
