import 'package:flutter/material.dart';

void showAboutApp(BuildContext context) {
  showAboutDialog(
    context: context,
    applicationIcon: Image.asset(
      'assets/images/logo.jpg',
      width: 50,
      height: 50,
    ),
    applicationName: 'Gustilandia App',
    applicationVersion: '1.0.1',
    applicationLegalese: '©2021 Khronos.net',
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(top: 15),
        child: Text(
            'Gustilandia está desarrollada por Khronos, un equipo de trabajo dedicado y comprometido a crear aplicaciones con las mejores tecnologías'),
      )
    ],
  );
}
