import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, text) {
  final snackbar = SnackBar(
    content: Text(text),
    duration: Duration(milliseconds: 1500),
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(50))),
    margin: EdgeInsets.symmetric(vertical: 50), //opcional
    elevation: 7,
    behavior: SnackBarBehavior.floating,
  );

  displaySnackBar(context: context, snackbar: snackbar);
}

void displaySnackBar({@required BuildContext context, SnackBar snackbar}) {
  ScaffoldMessenger.of(context).showSnackBar(snackbar);
}
