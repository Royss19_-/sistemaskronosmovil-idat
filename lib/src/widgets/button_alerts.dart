import 'package:flutter/material.dart';

Widget buttonAlert(BuildContext context, String text, Color bgColorBtn,
    Color colorTextBtn, VoidCallback callback) {
  return ElevatedButton(
    child: Container(
      padding: EdgeInsets.all(15),
      child: Text(
        text,
        style: TextStyle(fontSize: 20),
      ),
    ),
    style: ButtonStyle(
      //las propiedades seran configuradas con MaterialStateProperty
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      ),
      backgroundColor: MaterialStateProperty.all(bgColorBtn),
      foregroundColor: MaterialStateProperty.all(colorTextBtn),
    ),
    onPressed: callback,
  );
}
